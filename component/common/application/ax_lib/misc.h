/*
 *********************************************************************************
 *     Copyright (c) 2015	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 *********************************************************************************
 */
 /*============================================================================
 * Module name: misc.h
 * Purpose:
 * Author:
 * Date:
 * Notes:
 *
 *=============================================================================
 */

#ifndef __MISC_H__
#define __MISC_H__

/* INCLUDE FILE DECLARATIONS */

/* NAMING CONSTANT DECLARATIONS */
#define DEFAULT_WIFI_DISCONN_CHECK_TIMEOUT	5//(unit in 1 sec)
#define DEFAULT_WIFI_AUTOCONN_INTERVAL		10//(unit in 1 sec)
#define DEFAULT_IMAGE_RELOAD_TIMEOUT		15000//15 sec(unit in 1 msec)
#define DEFAULT_IMAGE_RELOAD_BUTTON			PE_3
#define DEFAULT_IMAGE_RELOAD_INDICATOR_LED	PE_0
   
typedef enum {
  	NETWORK_SWITCH_IDLE = 0,
	NETWORK_SWITCH_TO_BACKUP_IF,
	NETWORK_SWITCH_TO_ORIGINAL_IF,
} NETWORK_BACKUP_SWITCH_EVENTS, NETWORK_BACKUP_SWITCH_STATUS;

typedef enum {
	DHCPC_ADDRESS_ASSIGNED = 0,
	DHCPC_SET_STATIC,
	DHCPC_RELEASE_IP,	
	DHCPC_TIMEOUT,
	DHCPC_ERROR,	
	DHCPC_STOP,
  	DHCPC_START,
  	DHCPC_ENFORCED_START,	
	DHCPC_WAIT_ADDRESS,
} DHCP_CLIENT_STATE;

/* MACRO DECLARATIONS */

/* TYPE DECLARATIONS */

/* GLOBAL VARIABLES */

/* EXPORTED SUBPROGRAM SPECIFICATIONS */
NETWORK_BACKUP_SWITCH_STATUS MISC_GetNetworkBackupSwitchStatus(void);
void MISC_BackupNetworkDhcpStart(void);
void MISC_NetworkBackupSwitch(u8 enable, u8 backupNetId);
void MISC_DefaultImageReloadCheck(u16 ReloadTimeout, u8 ReloadButton, u8 IndicateLED);
u8 MISC_GetSerialNum(u8 *pdst, u8 dstlen);
void MISC_EnterWiFiBusySection(void);
void MISC_LeaveWiFiBusySection(void);
u32 MISC_GetCurrentIPAddr(void);
u8 MISC_LockFwUpgradeSection(void);
void MISC_UnLockFwUpgradeSection(void);
u8 MISC_DHCP(u8 NetIfId, u8 Ctrl, u8 BlockMode);
void MISC_SetWifiAutoConnInterval(u16 sec);
void MISC_SetWifiDisconnTimeout(u16 sec);
void MISC_GetMacAddr(u8 *pdst);
void MISC_SetWifiAutoConn(u8 Enable);
int MISC_GetRandom(void *p_rng, unsigned char *prng, size_t rnglen);
#endif /* __MISC_H__ */


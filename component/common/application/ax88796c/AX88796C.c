/*
 ******************************************************************************
 *     Copyright (c) 2015	ASIX Electronic Corporation      All rights reserved.
 *
 *     This is unpublished proprietary source code of ASIX Electronic Corporation
 *
 *     The copyright notice above does not evidence any actual or intended
 *     publication of such source code.
 ******************************************************************************
 */
/*=============================================================================
 * Module Name: ax88796C.c
 * Purpose:
 * Author:
 * Date:
 * Notes:
 *
 *=============================================================================
 */

/* INCLUDE FILE DECLARATIONS */
#include "FreeRTOS.h"
#include "semphr.h"
#include "AX88796C.h"
#include <stdio.h>
#include "gpio_api.h"
#include "gpio_irq_api.h"
#include "spi_api.h"
#include "spi_ex_api.h"

/* NAMING CONSTANT DECLARATIONS */
#define SPI0_MOSI  	PC_2
#define SPI0_MISO  	PC_3
#define SPI0_SCLK  	PC_1
#define SPI0_CS    	PC_0
#define SPI0_CS_M   PC_5
#define SPI0_IRQ   	PC_5

/* EXTERNAL VARIABLES DECLARATIONS */
extern xSemaphoreHandle	spi0_mutex;
	
/* GLOBAL VARIABLES DECLARATIONS */
spi_t spi0_master;

/* LOCAL VARIABLES DECLARATIONS */
static gpio_t ax88796_miso, ax88796_csm;
static unsigned char ax88796c_txbuf[AX88796C_TX_BUF_SIZE], ax88796c_rxbuf[AX88796C_RX_BUF_SIZE];
static unsigned short seq_num = 0;
static unsigned char ax88796_dbg = 1;

/* LOCAL SUBPROGRAM DECLARATIONS */
static unsigned short ax88796c_read_reg(unsigned char reg);
static void ax88796c_write_reg(unsigned short value, unsigned char reg);
static void ax88796c_write_fifo_pio(unsigned char *buf, unsigned short count);
static void ax88796c_read_fifo_pio(unsigned char *buf, unsigned short count);
static unsigned short ax88796c_mdio_read(unsigned short phy_id, unsigned short loc);
static void ax88796c_mdio_write(unsigned short phy_id, unsigned short loc, unsigned short val);
static void ax88796c_phy_init(void);

/* LOCAL SUBPROGRAM BODIES */

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_read_reg()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
static unsigned short ax88796c_read_reg(unsigned char reg)
{
	unsigned char txbuf[6], rxbuf[6];
	unsigned long val;
	
	txbuf[0] = AX_SPICMD_READ_REG;
	txbuf[1] = reg;
	txbuf[2] = 0xff;
	txbuf[3] = 0xff;
	
	while(xSemaphoreTake(spi0_mutex, portMAX_DELAY) != pdTRUE);	
	spi_flush_rx_fifo(&spi0_master);
	gpio_write(&ax88796_csm, 0);
	val = spi_master_write_read_stream(&spi0_master, txbuf, rxbuf, 6);	
	while (spi_busy(&spi0_master));	
	gpio_write(&ax88796_csm, 1);
	xSemaphoreGive(spi0_mutex);

#if (AX88796C_DEBUG_ENABLE > 1)
	if (ax88796_dbg)
	{
		printf("AX88796C: read reg[%02x] ", reg);	
		if (!val)
		{
			printf("= 0x%02x%02x OK\r\n", rxbuf[5], rxbuf[4]);	  
		}
		else if (val != HAL_BUSY)
		{
			printf("FAIL\r\n", val);
		}
		else
		{
			printf("\r\n");
		}
	}
#endif
	
	return (unsigned short)((rxbuf[5]<<8) + rxbuf[4]);
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_write_reg()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
static void ax88796c_write_reg(unsigned short value, unsigned char reg)
{
	unsigned char txbuf[4];
	unsigned long val;
	
	txbuf[0] = AX_SPICMD_WRITE_REG;
	txbuf[1] = reg;
	txbuf[2] = (u8)value;
	txbuf[3] = (u8)(value >> 8);
	
	while(xSemaphoreTake(spi0_mutex, portMAX_DELAY) != pdTRUE);		
	gpio_write(&ax88796_csm, 0);	
	val = spi_master_write_stream(&spi0_master, txbuf, 4);
	while (spi_busy(&spi0_master));
	gpio_write(&ax88796_csm, 1);	
	xSemaphoreGive(spi0_mutex);
	
#if (AX88796C_DEBUG_ENABLE > 1)
	if (ax88796_dbg)
	{	
		printf("AX88796C: write reg[%02x] = 0x%02x%02x ", reg, txbuf[3], txbuf[2]);	
		if (!val)
		{
			printf("OK\r\n");
		}
		else if (val != HAL_BUSY)
		{
			printf("FAIL\r\n");
		}
		else
		{
			printf("\r\n");	  
		}
	}
#endif	
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_write_fifo_pio()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
static void ax88796c_write_fifo_pio(unsigned char *buf, unsigned short count)
{
  	unsigned long val, i;
	unsigned short len;
	
	while(xSemaphoreTake(spi0_mutex, portMAX_DELAY) != pdTRUE);
	buf[0] = AX_SPICMD_WRITE_TXQ;
	buf[1] = 0xff;
	buf[2] = 0xff;
	buf[3] = 0xff;
 	len = count + AX88796C_FIFO_WRITE_OFFSET;
	if (len & 0x01)
		len++;
	
	gpio_write(&ax88796_csm, 0);
	val = spi_master_write_stream(&spi0_master, buf, len);
	while (spi_busy(&spi0_master));	
	gpio_write(&ax88796_csm, 1);
	xSemaphoreGive(spi0_mutex);
	if (len & 0x01)
	{
		printf("AX88796C: tx even pkt(%d)\r\n", len);
	}	
#if AX88796C_DEBUG_ENABLE
	count -= AX88796C_TX_SOP_SEG_HDR_LEN + AX88796C_TX_EOP_HDR_LEN;
	printf("AX88796C: tx pkt(%d)=0x", count);
	for (i=0; i<count; i++)
	{
		printf("%02x", buf[i + AX88796C_FIFO_WRITE_OFFSET + AX88796C_TX_SOP_SEG_HDR_LEN]);
	}
	printf(" OK\r\n");
#endif	
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_read_fifo_pio()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
static void ax88796c_read_fifo_pio(unsigned char *buf, unsigned short count)
{
	unsigned char txbuf[5];  
  	unsigned long val, i;
	unsigned short len;
	
	while(xSemaphoreTake(spi0_mutex, portMAX_DELAY) != pdTRUE);	
	txbuf[0] = AX_SPICMD_READ_RXQ;
	txbuf[1] = 0xff;
	txbuf[2] = 0xff;
	txbuf[3] = 0xff;
	txbuf[4] = 0xff;
 	len = count + AX88796C_FIFO_READ_OFFSET;
	if (len & 0x01)
		len++;
	
	spi_flush_rx_fifo(&spi0_master);
	gpio_write(&ax88796_csm, 0);
   	val = spi_master_write_read_stream(&spi0_master, txbuf, buf, len);
	while (spi_busy(&spi0_master));	
	gpio_write(&ax88796_csm, 1);	
	xSemaphoreGive(spi0_mutex);
	if (len & 0x01)
	{
		printf("AX88796C: rx odd pkt(%d)\r\n", len);
	}
#if AX88796C_DEBUG_ENABLE
	count -= AX88796C_RX_HDR_LEN;
	printf("AX88796C: rx pkt(%d)=0x", count);
	for (i=0; i<count; i++)
	{
		printf("%02x", buf[i+AX88796C_FIFO_READ_OFFSET+AX88796C_RX_HDR_LEN]);
	}
	if (!val)
	{
		printf(" OK\r\n");
	}
	else if (val != HAL_BUSY)
	{
		printf(" FAIL\r\n");
	}
	else
	{
		printf(" \r\n");
	}
#endif
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_mdio_read()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
static unsigned short ax88796c_mdio_read(unsigned short phy_id, unsigned short loc)
{
	unsigned short timeout;

	ax88796c_write_reg((MDIOCR_RADDR(loc) | MDIOCR_FADDR(phy_id) | MDIOCR_READ), P2_MDIOCR);
	for (timeout = 0; timeout < 10; timeout++)
	{
		if (ax88796c_read_reg(P2_MDIOCR) & MDIOCR_VALID)
			break;
		vTaskDelay(1);
	}
	return (ax88796c_read_reg(P2_MDIODR));
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_mdio_write()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
static void ax88796c_mdio_write(unsigned short phy_id, unsigned short loc, unsigned short val)
{
	unsigned short timeout;

	ax88796c_write_reg(val, P2_MDIODR);
	ax88796c_write_reg((MDIOCR_RADDR(loc) | MDIOCR_FADDR(phy_id) | MDIOCR_WRITE), P2_MDIOCR);

	for (timeout = 0; timeout < 10; timeout++)
	{
		if (ax88796c_read_reg(P2_MDIOCR) & MDIOCR_VALID)
			break;
		vTaskDelay(1);
	}
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_phy_init()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
void ax88796c_phy_init(void)
{
	unsigned short tmp16;

	/* Setup LED mode */
	ax88796c_write_reg((LCR_LED0_EN | LCR_LED0_DUPLEX | LCR_LED1_EN | LCR_LED1_100MODE), P2_LCR0);

	tmp16 = ax88796c_read_reg(P2_LCR1);
	ax88796c_write_reg((tmp16 & LCR_LED2_MASK) | LCR_LED2_EN | LCR_LED2_LINK, P2_LCR1);

	tmp16 = (POOLCR_PHYID(0x10) | POOLCR_POLL_EN | POOLCR_POLL_FLOWCTRL | POOLCR_POLL_BMCR);
	ax88796c_write_reg(tmp16, P2_POOLCR);

	ax88796c_mdio_write(AX88796C_PHY_ID, PHY_REG_AN_ADVERTISEMENT, SPEED_DUPLEX_AUTO);
	ax88796c_mdio_write(AX88796C_PHY_ID, PHY_REG_BASIC_MODE_CTRL, RESTART_AUTONEG);
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_spi_init()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
void ax88796c_spi_init(void)
{
	/* AX88796 driver initialization */
	spi0_mutex = xSemaphoreCreateMutex();
	
	/* SPI hardware configuration */	
	gpio_init(&ax88796_miso, SPI0_MISO);
    GpioFunctionChk(SPI0_MISO, DISABLE);
	gpio_mode(&ax88796_miso, PullDefault);
	gpio_dir(&ax88796_miso, PIN_INPUT);

#if AX88796C_MANUAL_SLAVE_SELECT
	gpio_init(&ax88796_csm, SPI0_CS_M);
    GpioFunctionChk(SPI0_CS_M, DISABLE);
	gpio_dir(&ax88796_csm, PIN_OUTPUT);
	gpio_mode(&ax88796_csm, PullUp);
	gpio_write(&ax88796_csm, 1);	
#endif

    spi_init(&spi0_master, SPI0_MOSI, SPI0_MISO, SPI0_SCLK, SPI0_CS);
	spi_format(&spi0_master, 8, 3, 0);
	spi_frequency(&spi0_master, AX88796C_SPI_SPEED);
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_init()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
signed short ax88796c_init(unsigned char *phwaddr)
{
	unsigned short tmp16;
	unsigned long tmp32;
	
	/* Chip Reset */
	ax88796c_write_reg(PSR_RESET, P0_PSR);	
	tmp32 = 10;
	while (tmp32--);
	ax88796c_write_reg(PSR_RESET_CLR, P0_PSR);

	/* Make sure AX88796C is ready */
	tmp16 = 5;		
	while (1)
	{
		if ((ax88796c_read_reg(P0_BOR) == 0x1234) && (ax88796c_read_reg(P0_PSR) & PSR_DEV_READY))
		{
			break;
		}
		else if (tmp16 == 0)
		{
			printf("AX88796C: Fail to wait for chip ready.\r\n");
			return -1;
		}
		else
		{
			ax88796c_write_reg(PSR_RESET, P0_PSR);	
			tmp32 = 10;	
			while (tmp32--);
			ax88796c_write_reg(PSR_RESET_CLR, P0_PSR);
		}
		tmp16--;
		vTaskDelay(100);		
	}
	
	/* Register compresion = 0 */
	tmp16 = ax88796c_read_reg(P4_SPICR) & (~(SPICR_RCEN | SPICR_QCEN));
	ax88796c_write_reg(tmp16, P4_SPICR);

	/* Set mac for AX88796C chip */
	ax88796c_set_mac_addr(phwaddr);

	/* Enable crc checksum supporting */
	ax88796c_write_reg((COERCR0_RXIPCE | COERCR0_RXTCPE | COERCR0_RXUDPE), P4_COERCR0);
	ax88796c_write_reg((COETCR0_TXIP | COETCR0_TXTCP | COETCR0_TXUDP), P4_COETCR0);

	/* Enable stuffing packet */
	tmp16 = ax88796c_read_reg(P1_RXBSPCR) | RXBSPCR_STUF_ENABLE;
	ax88796c_write_reg(tmp16, P1_RXBSPCR);

	/* Enable RX packet process */
	ax88796c_write_reg(RPPER_RXEN, P1_RPPER);

	/* Set INT pin */
	tmp16 = ax88796c_read_reg(P0_FER) | FER_RXEN | FER_TXEN | FER_BSWAP | FER_IRQ_PULL | FER_INTLO;
	ax88796c_write_reg(tmp16, P0_FER);

	/* Set PHY */
	ax88796c_phy_init();
  
	/* Enable interrupt */
	ax88796c_write_reg(~(IMR_LINK | IMR_RXPKT), P0_IMR);
	
	return 0;
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_set_mac_addr()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
void ax88796c_set_mac_addr(unsigned char *phwaddr)
{
	unsigned short tmp16;

	tmp16 = ((unsigned short)(phwaddr[4] << 8) | (unsigned short)(phwaddr[5]));
	ax88796c_write_reg(tmp16, P3_MACASR0);

	tmp16 = ((unsigned short)(phwaddr[2] << 8) | (unsigned short)(phwaddr[3]));
	ax88796c_write_reg(tmp16, P3_MACASR1);

	tmp16 = ((unsigned short)(phwaddr[0] << 8) | (unsigned short)(phwaddr[1]));
	ax88796c_write_reg(tmp16, P3_MACASR2);
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_load_mac_addr()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
void ax88796c_load_mac_addr(unsigned char *phwaddr)
{
	unsigned short tmp16;

	tmp16 = ax88796c_read_reg(P3_MACASR0);
	phwaddr[5] = (unsigned char) tmp16;
	phwaddr[4] = (unsigned char)(tmp16 >> 8);

	tmp16 = ax88796c_read_reg(P3_MACASR1);
	phwaddr[3] = (unsigned char) tmp16;
	phwaddr[2] = (unsigned char)(tmp16 >> 8);

	tmp16 = ax88796c_read_reg(P3_MACASR2);
	phwaddr[1] = (unsigned char) tmp16;
	phwaddr[0] = (unsigned char)(tmp16 >> 8);
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_pkt_send()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
signed short ax88796c_pkt_send(struct buf_list *plist, unsigned char list_depth, unsigned short pkt_len)
{
	unsigned char i, align_count;
	unsigned short tmp16, len_bar;
	unsigned long tmp32;
	unsigned char *ptxbuf = ax88796c_txbuf + AX88796C_FIFO_WRITE_OFFSET;
	
	/* Check target buffer is valid */	
	if (plist == 0 || 
		list_depth == 0 ||
		(AX88796C_FIFO_WRITE_OFFSET + AX88796C_TX_SOP_SEG_HDR_LEN + pkt_len + 4 + AX88796C_TX_EOP_HDR_LEN) >= AX88796C_TX_BUF_SIZE)	
	{
		printf("AX88796C: Wrong tx buffer\r\n");
		return -1;	
	}

	len_bar = (~pkt_len & TX_HDR_SOP_PKTLENBAR);
	/* Build SOP header */
	*((unsigned short*)&ptxbuf[0]) = SWAP16(pkt_len);
	*((unsigned short*)&ptxbuf[2]) = SWAP16(TX_HDR_SEQNUM(seq_num) | len_bar);
			
	/* Build Segment header */
	*((unsigned short*)&ptxbuf[4]) = SWAP16(TX_HDR_SEG_FS | TX_HDR_SEG_LS | pkt_len);
	*((unsigned short*)&ptxbuf[6]) = SWAP16(len_bar);
	ptxbuf += AX88796C_TX_SOP_SEG_HDR_LEN;
	
	/* Fill packet content */
	for (i=0; i<list_depth; i++)
	{
		memcpy(ptxbuf, plist[i].pbuf, plist[i].len);
		ptxbuf += plist[i].len;
	}

	/* Build padding for double-word alignment */		  
	align_count = ((pkt_len + 3) & 0x7FC) - pkt_len;
	if (align_count)
	{
		memset(ptxbuf, 0xff, align_count);	  
  		ptxbuf += align_count;
	}
	
	/* Build EOP header */
	*((unsigned short*)&ptxbuf[0]) = SWAP16(TX_HDR_SEQNUM(seq_num) | pkt_len);
	*((unsigned short*)&ptxbuf[2]) = SWAP16(TX_HDR_SEQNUM(~seq_num) | len_bar);
	ptxbuf += AX88796C_TX_EOP_HDR_LEN;
	
	/* Increase sequence number */
	seq_num++;
	seq_num &= 0x1f;
	
	/* Write TX end of packet */
	ax88796c_write_reg((TSNR_TXB_START | TSNR_PKT_CNT(1)), P0_TSNR);
	ax88796c_write_fifo_pio(ax88796c_txbuf, AX88796C_TX_SOP_SEG_HDR_LEN + pkt_len + align_count + AX88796C_TX_EOP_HDR_LEN);
	
	/* Check result */
	tmp32 = 10;
	while ((ax88796c_read_reg(P0_PSR)&PSR_DEV_READY) == 0)
	{
		if (tmp32 == 0)
		{
			printf("AX88796C: TX check ready timeout\r\n");		  
			goto low_level_tx_err;
		}
		tmp32--;
		vTaskDelay(1);
	}
	
	tmp16 = ax88796c_read_reg(P0_ISR);
	if (tmp16 & ISR_TXERR)
	{
		printf("AX88796C: TX fail\r\n");
		goto low_level_tx_err;
	}		
	return 0;

low_level_tx_err:
	ax88796c_write_reg(TXNR_TXB_REINIT, P0_TSNR);
	seq_num	= 0;
	return -1;
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_check_rev_pkt_count()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
signed short ax88796c_check_rev_pkt_count(unsigned short mtu)
{
	unsigned short tmp16;
	
	/* Latch RX bridge first */  
	tmp16 = ax88796c_read_reg(P0_RTWCR) | RTWCR_RX_LATCH;
	ax88796c_write_reg(tmp16, P0_RTWCR);
	
	/* Get received packet count */  	
	if ((ax88796c_read_reg(P0_RXBCR2) & RXBCR2_PKT_MASK) == 0)
	{
		return 0;
	}

	/* Get size of topest packet */
	tmp16 = ax88796c_read_reg(P0_RCPHR);
	if (tmp16 & RX_HDR_ERROR)
	{
		/* Skip the packet which has header error */
		ax88796c_write_reg(RXBCR1_RXB_DISCARD, P0_RXBCR1);
		return 0;
	}

	/* Check packet length */
	tmp16 &= 0x7FF;
//	if ((tmp16 < 60) || (tmp16 > mtu))	
	if ((tmp16 < 60))		
	{
		/* Discard the packet which length is invalid */	  
		ax88796c_write_reg(RXBCR1_RXB_DISCARD, P0_RXBCR1);
#if (AX88796C_DEBUG_ENABLE > 1)
		printf("AX88796C: Packet size is either larger than %d or small 60(%d).\n", mtu, tmp16);
#endif		
		return 0;
	}
	
	return tmp16;
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_pkt_recv()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
signed short ax88796c_pkt_recv(struct buf_list *plist, unsigned char list_depth, unsigned short pkt_len)
{
	unsigned short tmp16;
	unsigned long tmp32;
	unsigned char i, *prxbuf = ax88796c_rxbuf + AX88796C_FIFO_READ_OFFSET + AX88796C_RX_HDR_LEN;

	/* Check target buffer is valid */
	if (plist == 0 || 
		list_depth == 0 ||
		(AX88796C_FIFO_READ_OFFSET + AX88796C_RX_HDR_LEN + pkt_len + 4) >= AX88796C_RX_BUF_SIZE)
	{
		printf("AX88796C: Wrong rx buffer\r\n");	  
		return -1;
	}
	
	/* Start RX bridge for data transfer */
	ax88796c_write_reg((RXBCR1_RXB_START | (pkt_len >> 1)), P0_RXBCR1);
	tmp32 = 10;
	while ((ax88796c_read_reg(P0_RXBCR2) & RXBCR2_RXB_READY) == 0)
	{
		if (tmp32 == 0)
		{
			ax88796c_write_reg(RXBCR1_RXB_DISCARD, P0_RXBCR1);
			return -1;
		}
		tmp32--;
		vTaskDelay(1);
	}

	/* Read full packet (includes header + packet content) */
    ax88796c_read_fifo_pio(ax88796c_rxbuf, pkt_len);
	
	/* Read packet content */
    for (i=0; i<list_depth; i++)
    {
		memcpy(plist[i].pbuf, prxbuf, plist[i].len);
		prxbuf += plist[i].len;
	}
	
	/* Check RX bridge read operation is done */
	tmp32 = 10;
	while ((ax88796c_read_reg(P0_RXBCR2) & RXBCR2_RXB_IDLE) == 0)
	{
		if (tmp32 == 0)
		{
			ax88796c_write_reg (RXBCR2_RXB_REINIT, P0_RXBCR2);
			return -1;
		}
		tmp32--;
		vTaskDelay(1);
	}
	return 0;
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_pkt_drop()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
void ax88796c_pkt_drop(unsigned short pkt_len)
{
	unsigned short tmp16;
	unsigned long tmp32;
	unsigned char *prxbuf = ax88796c_rxbuf + AX88796C_FIFO_READ_OFFSET + AX88796C_RX_HDR_LEN;

	if ((AX88796C_FIFO_READ_OFFSET + AX88796C_RX_HDR_LEN + pkt_len + 4) >= AX88796C_RX_BUF_SIZE)
	{
		printf("ax88796c_pkt_drop: Wrong pkt_len > %d\r\n", AX88796C_RX_BUF_SIZE);	  
		return;
	}

	/* Start RX bridge for data transfer */
	ax88796c_write_reg((RXBCR1_RXB_START | (pkt_len >> 1)), P0_RXBCR1);
	tmp32 = 10;
	while ((ax88796c_read_reg(P0_RXBCR2) & RXBCR2_RXB_READY) == 0)
	{
		if (tmp32 == 0)
		{
			ax88796c_write_reg(RXBCR1_RXB_DISCARD, P0_RXBCR1);
			return;
		}
		tmp32--;
		vTaskDelay(1);
	}

	/* Read full packet (includes header + packet content) */
    ax88796c_read_fifo_pio(ax88796c_rxbuf, pkt_len);

	/* Check RX bridge read operation is done */
	tmp32 = 10;
	while ((ax88796c_read_reg(P0_RXBCR2) & RXBCR2_RXB_IDLE) == 0)
	{
		if (tmp32 == 0)
		{
			ax88796c_write_reg (RXBCR2_RXB_REINIT, P0_RXBCR2);
			break;
		}
		tmp32--;
		vTaskDelay(1);
	}
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_get_event()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
AX88796C_EVENT_TYPES ax88796c_get_event(void)
{
	unsigned short isr, events = 0;

	ax88796_dbg	= 0;
	
	isr = ax88796c_read_reg(P0_ISR);
	ax88796c_write_reg(isr, P0_ISR);/* Clear event flag */
	
	if (isr & ISR_RXPKT)
	{
		events |= EVENT_RX_PKT;
	}
	
	if (isr & ISR_LINK)
	{
		if (ax88796c_mdio_read(AX88796C_PHY_ID, PHY_REG_BASIC_MODE_STATUS) & 0x0004)
		{
	  		events |= EVENT_LINK_UP;
		}
		else
		{
			events |= EVENT_LINK_DOWN;		  
		}
	}
	
	ax88796_dbg	= 1;	
	return events;
}

/*
 * ----------------------------------------------------------------------------
 * Function: ax88796c_get_mac_cfg()
 * Purpose :
 * Params  :
 * Returns :
 * Note    :
 * ----------------------------------------------------------------------------
 */
void ax88796c_get_mac_cfg(unsigned char *full_duplex, unsigned short *speed, unsigned char *rx_flow_ctrl, unsigned char *tx_flow_ctrl)
{
	unsigned short mac_cfg;

	mac_cfg = ax88796c_read_reg(P0_MACCR);
	
	*full_duplex = (mac_cfg & MACCR_DUPLEXFULL) ? 1:0;
	*speed = (mac_cfg & MACCR_SPEED100) ? 100:10;	
	*rx_flow_ctrl = (mac_cfg & MACCR_RXFC_ENABLE) ? 1:0;		
	*tx_flow_ctrl = (mac_cfg & MACCR_TXFC_ENABLE) ? 1:0;
}

/* End of ax88796c.c */